#!/usr/bin/perl
use warnings;
use strict;

use autotrade::MarketTools;

use autotrade::SQL;
use autotrade::RateHistory;

my $db = autotrade::SQL::db_connect();
my $rh = autotrade::RateHistory->new(dbh => $db);
my $lt = 0;
$db->do('BEGIN');
while (<>) {
	chomp;
	my ($t, $buy, $amount) = split(/,/);
	$t - $lt > 20 or next;
	$lt = $t;
	$buy = str_curr($buy);
	print "$t $buy ($amount)\n";
	$rh->record($t, $buy, $buy);
}
$db->do('COMMIT');
