package autotrade::Trader::LowHigh;

# A trader that simply buys low and sells higher, inefficiently.

# Algorithm:
#
# We simply aim to generate profit by buying bitcoints and selling them
# at somewhat higher price to generate at least required margin. Each
# transaction is $bamount BTC and we seek to gain profit at least
# $sellmargin USD on it, therefore placing a sell order at $bamount*rate
# + fees + $sellmargin.
#
# Problems:
# 1. We buy coins immediately after selling them, incurring huge transaction
# costs.
# 2. We get blocked at local maximum, helpless when BTC start falling.
#
# Possible solutions:
# 1. Follow derivation over multiple timeframes.
# 2. Re-targetting old sell orders to avoid long-term blocks.
# 3. When price is dropping, sell part of investment if passing original
# value, so that we can re-invest in valley.
#
#
# We expire old buy orders. If it is older than buy_lifetime seconds, throw
# it away, it is not going to happen.
#
# TODO:
# We also expire (re-target to current settings) old sell orders.
# We compute the expiration loss (we bought for X, now we are selling
# for less than X + margin), expected transaction loss (we expect to
# do $exptransperday successful transactions per day, how much we could
# make if the money was not sitting on idle sell order for all this time?)
# and when the ratio goes over $resellratio (say, 1) we accept the loss
# and sell the bitcoins to return them to circulation.
#
# Fee model:
#
# MtGox charges $tfee (0.0065) per transaction from the transaction value,
# i.e. the whole amount being charged.  In a sell order (and re-targeting
# old sell orders!), we must top the target with fee compensation. E.g. for
# 20USD w/ 0.1USD profit margin, fees amount to 0.24USD!.

# TODO: Better buy price selection based on market depth.
# TODO: Auto-adjust $sellmargin based on volatility?
# TODO: Auto-adjust $interval based on volatility?

use Moose;
extends 'autotrade::Trader';
use autotrade::MarketTools;

# [BTC] Transaction size
has 'btc_amount' => ( isa => 'Curr', is => 'ro', coerce => 1, default => str_curr(0.618) );

# [USD] Sell order above current rate plus this margin; this * btc_amount is how much we make per transaction (up to fees!).
has 'sell_margin' => ( isa => 'Curr', is => 'ro', coerce => 1, default => str_curr(0.1) );

has '+buy_lifetime' => ( isa => 'Int', is => 'rw', default => 60 );


override beat => sub {
	my $self = shift;
	my ($md) = @_; # autotrade::MarketData

	super();

	# First, check balance.
	my $ebalance = $md->tradeable_balance();
	printf "\tEffective balance %s\n", curr_str($ebalance);
	$ebalance > 0 or return 0;

	# Figure out goal rate.
	my $brate = $md->rate_sellbtc(); # buy immediately from the lowest existing sell order
	my $srate = $brate + $self->sell_margin();
	my $fees = doublefee_cost($self->m(), $brate, $srate);
	$srate += $fees;
	printf "\t\tCurrent buy rate %s, our buy rate %s, fees %s, sell rate %s\n",
		curr_str($md->rate_buybtc()), curr_str($brate), curr_str($fees), curr_str($srate);

	# Print the profit projection and check balance again.
	my $usd_amount_buy = curr_conv($self->btc_amount(), $brate);
	my $usd_amount_sell = curr_conv($self->btc_amount(), $srate);
	printf "\t\tWould buy %s BTC for %s USD, sell for %s USD\n",
		curr_str($self->btc_amount()), curr_str($usd_amount_buy), curr_str($usd_amount_sell);
	if ($ebalance <= $usd_amount_buy) {
		print "\tNot enough funds.\n";
		return 0;
	}

	# Make the order to buy BTC and sell them later, for determined rates.
	$self->m()->buy($self->btc_amount(), $brate);
	$self->m()->sell($self->btc_amount(), $srate);
	print "\t* Orders PLACED. *\n";

	# Check if we can trade more!
	return 1;
};

no Moose;
__PACKAGE__->meta->make_immutable;
