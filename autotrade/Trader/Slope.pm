package autotrade::Trader::Slope;

# A trader that buys and sells based on price curve shape.

# Algorithm:
#
# We again aim to buy low and sell high. But unlike LowHigh, we:
# 1. Buy only when the price is raising.
# 2. Sell only when the price is falling.
# We do not have any positive sell margin, but we do not sell at loss.
# Of course, the difficult question is what is "raising" and "falling",
# there may be short-term and long-term trends. We simply compare current
# price to price $time_window seconds ago; it is good idea to have
# multiple accounts for following trends of various temporality.
#
# To prevent not selling at loss, we keep track of all "unsold buys"
# and their price. When price falls, we sell all unsold buys below
# current rate, in the order from most expensive ones.
#
# Problems:
# 1. We may get blocked at too tiny local maximum, helpless when BTC
# start falling.
#
# Possible solutions:
# 1. Re-targetting old sell orders to avoid long-term blocks.
# 2. When price is dropping, sell part of investment if passing original
# value, so that we can re-invest in valley.
#
#
# We always buy at market's sale rate and sell at market's buy rate.
# Therefore, do not be confused by some of the buy-sell crossovers.
#
# We expire old buy orders. If it is older than buy_lifetime seconds, throw
# it away, it is not going to happen.
#
# TODO:
# We also expire (re-target to current settings) old sell orders.
# We compute the expiration loss (we bought for X, now we are selling
# for less than X + margin), expected transaction loss (we expect to
# do $exptransperday successful transactions per day, how much we could
# make if the money was not sitting on idle sell order for all this time?)
# and when the ratio goes over $resellratio (say, 1) we accept the loss
# and sell the bitcoins to return them to circulation.
#
# Fee model:
#
# MtGox charges $tfee (0.0065) per transaction from the transaction value,
# i.e. the whole amount being charged.  In a sell order (and re-targeting
# old sell orders!), we must top the target with fee compensation. E.g. for
# 20USD w/ 0.1USD profit margin, fees amount to 0.24USD!.

# TODO: Better buy price selection based on market depth.

use Moose;
extends 'autotrade::Trader';
use autotrade::MarketTools;

has 'dbh' => ( isa => 'Object', is => 'ro', required => 1 );
has 'rh' => ( isa => 'autotrade::RateHistory', is => 'ro', required => 1 );

# [BTC] Transaction size
has 'btc_amount' => ( isa => 'Curr', is => 'ro', coerce => 1, default => str_curr(0.1) );

# [s] How far in the past to look at trending.
has 'time_window' => ( isa => 'Int', is => 'ro', default => 120 );

# [USD/s] Minimal raising rate to start buying.
has 'raise_threshold' => ( isa => 'Num', is => 'ro', default => 1.0 );

# [USD/s] Maximal falling rate to start selling.
has 'fall_threshold' => ( isa => 'Num', is => 'ro', default => -0.5 );

# [USD] Extra sell margin below current sell rate, to ensure smooth sale.
has 'undersell_margin' => ( isa => 'Num', is => 'ro', default => str_curr(0.005) );

has '+buy_lifetime' => ( isa => 'Int', is => 'rw', default => 30 );
has '+sell_lifetime' => ( isa => 'Int', is => 'rw', default => 10 );

has 'qtotbalance' => ( isa => 'Object', is => 'ro', lazy => 1, default =>
			sub { $_[0]->dbh()->prepare("SELECT SUM(btc_amount) FROM bitcoin_values") } );
has 'qsellbalance' => ( isa => 'Object', is => 'ro', lazy => 1, default =>
			sub { $_[0]->dbh()->prepare("SELECT SUM(btc_amount) FROM bitcoin_values WHERE bought_at_rate <= ?") } );
has 'qbuys' => ( isa => 'Object', is => 'ro', lazy => 1, default =>
			sub { $_[0]->dbh()->prepare("SELECT rowid, btc_amount, bought_at_rate FROM bitcoin_values WHERE bought_at_rate <= ? ORDER BY bought_at_rate DESC") } );
has 'qcancelbuy' => ( isa => 'Object', is => 'ro', lazy => 1, default =>
			sub { $_[0]->dbh()->prepare("DELETE FROM bitcoin_values WHERE rowid == ?") } );
has 'qreducebuy' => ( isa => 'Object', is => 'ro', lazy => 1, default =>
			sub { $_[0]->dbh()->prepare("UPDATE bitcoin_values SET btc_amount = ? WHERE rowid == ?") } );
has 'qnewbuy' => ( isa => 'Object', is => 'ro', lazy => 1, default =>
			sub { $_[0]->dbh()->prepare("INSERT INTO bitcoin_values (btc_amount, bought_at_rate) VALUES (?, ?)") } );


# Prune database bitcoin records based on current BTC balance.
sub prune_by_balance {
	my $self = shift;
	my ($md, $sellrate) = @_;

	my $m_balance = $md->bal_btc();
	$self->qtotbalance()->execute();
	my $d_balance = ($self->qtotbalance()->fetchrow_array())[0];
	$d_balance ||= 0;
	my $delta = $d_balance - $m_balance;
	$delta == 0 or print "\t\tAccount balance ".curr_str($m_balance)." BTC, database balance ".curr_str($d_balance)." BTC\n";
	$delta > 0 or return;

	$self->qbuys()->execute($sellrate);
	$self->dbh()->do('BEGIN');
	while ($delta > 0 and my @row = $self->qbuys()->fetchrow_array) {
		my ($rowid, $btc_amount, $bought_at_rate) = @row;
		if ($delta >= $btc_amount) {
			print "\t\t- Pruning buy $rowid (".curr_str($btc_amount)." BTC @ ".curr_str($bought_at_rate).")\n";
			$self->qcancelbuy()->execute($rowid);
			$delta -= $btc_amount;
		} else {
			print "\t\t- Reducing buy $rowid (".curr_str($btc_amount)." BTC @ ".curr_str($bought_at_rate).") -= ".curr_str($delta)."\n";
			$self->qreducebuy()->execute($btc_amount - $delta, $rowid);
			$delta = 0;
		}
	}
	$self->dbh()->do('COMMIT');
}

# Cancel all sell orders and declare new sell offer.
sub sell_at {
	my $self = shift;
	my ($md, $amount, $rate) = @_;

	# First, cancel all outstanding sales.
	my @o = grep { $_->otype() eq 'sell' } @{$md->orders()};
	foreach my $o (@o) {
		printf "\t\t- Cancelling superseded order %d (%d): %s %s BTC @ %s USD\n",
			$o->oid(), $o->time(), $o->otype(), curr_str($o->amount()), curr_str($o->rate());
		$self->m()->cancel($o);
	}

	# Then, declare new sale.
	printf "\t\tMaking sale order: %s BTC @ %s\n", curr_str($amount), curr_str($rate);
	$self->m()->sell($amount, $rate);
}

sub mass_sell {
	my $self = shift;
	my ($md) = @_;

	my $sellrate = $md->rate_buybtc() - $self->undersell_margin();
	printf "\t* Selling at %s!\n", curr_str($sellrate);
	if ($md->bal_btc() == 0) {
		print "\t\tNothing to sell. Nice try.\n";
		return 0;
	}

	$self->prune_by_balance($md, $sellrate);

	$self->qsellbalance()->execute($sellrate);
	my $amount = ($self->qsellbalance()->fetchrow_array())[0];
	$amount ||= 0;
	if ($amount > 0) {
		$self->sell_at($md, $amount, $sellrate);
	}
	return 0;
}

sub buy_one {
	my $self = shift;
	my ($md) = @_;

	# XXX: We do not properly handle failed buy order!
	# XXX: Here, we do not properly compesate for higher sale fee, but it
	# should not affect the inequalities in sell logic.
	# XXX: Moreover, this is slightly pessimistic since we sell btc_amount
	# without the fee, thus paying less sell fee.
	#my $fee_btcamount = $self->btc_amount() * $self->m()->fee();
	my $fee_btcamount = 0;
	my $sellrate = $md->rate_sellbtc() + 2 * $md->rate_sellbtc() * $self->m()->fee() / (1 - $self->m()->fee());

	printf "\t* Buying %s BTC at %s (sell thres. %s)!\n",
		curr_str($self->btc_amount()), curr_str($md->rate_sellbtc()), curr_str($sellrate);

	my $ebalance = $md->tradeable_balance();
	if ($ebalance < curr_conv($self->btc_amount(), $md->rate_sellbtc())) {
		printf "\t\tInsufficient balance %s USD\n", curr_str($ebalance);
		return 0;
	}

	$self->m()->buy($self->btc_amount(), $md->rate_sellbtc());

	$self->qnewbuy()->execute($self->btc_amount() - $fee_btcamount, $sellrate);
	return 1;
}

override beat => sub {
	my $self = shift;
	my ($md, $time) = @_; # autotrade::MarketData

	super();

	# Determine current direction.
	my $slope = $self->rh()->deriv($self->time_window(), $self->time_window() / 2, $time);
	if (not defined $slope) {
		printf "\tNot enough data (maxlag %d)\n", $self->time_window() / 2;
		return 0;
	}
	printf "\tt-%d = %s,%s, t = %s,%s, slope %.3f,%.3f\n", $self->time_window(),
		curr_str($slope->[4]->[1]), curr_str($slope->[4]->[2]),
		curr_str($slope->[3]->[1]), curr_str($slope->[3]->[2]),
		$slope->[1], $slope->[2];

	# Action selection.
	if ($slope->[1] < $self->fall_threshold()) {
		# Sell bitcoins that still make profit.
		return $self->mass_sell($md);
	} elsif ($slope->[1] > $self->raise_threshold()) {
		# Buy a dime!
		return $self->buy_one($md);
	} else {
		# Quiet moment...
		print "\tToo quiet, staying put.\n";
		return 0;
	}
};

no Moose;
__PACKAGE__->meta->make_immutable;
