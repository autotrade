package autotrade::MarketData;

use Moose;
use autotrade::MarketTools;

has 'm' => (isa => 'autotrade::Market', is => 'ro', required => 1);

# Both balances and rates are *1000 integers. Use autotrade::MarketTools
# for manipulation with these.

# rwm == real-world money
has 'bal_btc' => (isa => 'Curr', is => 'ro', lazy_build => 1);
has 'bal_rwm' => (isa => 'Curr', is => 'ro', lazy_build => 1);

has 'rate_buybtc' => (isa => 'Curr', is => 'ro', lazy_build => 1);
has 'rate_sellbtc' => (isa => 'Curr', is => 'ro', lazy_build => 1);

has 'orders' => (isa => 'ArrayRef[autotrade::MarketData::Order]', is => 'ro', lazy_build => 1);

sub tradeable_balance {
	my $self = shift;
	my $balance = $self->bal_rwm();
	printf "\t\tBasic balance %s (and %s BTC)\n", curr_str($balance), curr_str($self->bal_btc());
	foreach my $o (grep { $_->otype() eq 'buy' } @{$self->orders()}) {
		printf "\t\t- Buy order %s @ %s\n", curr_str($o->amount()), curr_str($o->rate());
		$balance -= curr_conv($o->amount(), $o->rate());
	}
	$balance -= $self->m()->minbal_rwm();
	return $balance;
}

1;


package autotrade::MarketData::Order;

use Moose;
use Moose::Util::TypeConstraints;

has 'otype' => (isa => enum([qw(buy sell)]), is => 'ro', required => 1);
has 'oid' => (isa => 'Int', is => 'ro', required => 1);
has 'amount' => (isa => 'Curr', is => 'ro', coerce => 1, required => 1);
has 'rate' => (isa => 'Curr', is => 'ro', coerce => 1, required => 1);
has 'time' => (isa => 'Int', is => 'ro');

1;
