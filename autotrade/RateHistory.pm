package autotrade::RateHistory;

use Moose;
use List::MoreUtils;

has 'dbh' => ( isa => 'Object', is => 'ro', required => 1 );
has 'qrecord' => ( isa => 'Object', is => 'ro', lazy => 1, default =>
			sub { $_[0]->dbh()->prepare("INSERT INTO rate_history (time, buy, sell) VALUES (?, ?, ?)") } );
has 'qretrprev' => ( isa => 'Object', is => 'ro', lazy => 1, default =>
			sub { $_[0]->dbh()->prepare("SELECT time, buy, sell FROM rate_history WHERE time <= ? ORDER BY time DESC LIMIT 1") } );

sub record {
	my $self = shift;
	my ($time, $buy, $sell);
	if (@_ == 3) {
		($time, $buy, $sell) = (@_);
	} else { @_ == 2 or confess "store_frame bad usage";
		($time, $buy, $sell) = (time, @_);
	}
	$self->qrecord()->execute($time, $buy, $sell);
}

sub retrieve_at {
	my $self = shift;
	my ($time) = @_;
	$self->qretrprev()->execute($time);
	$self->qretrprev()->fetchall_arrayref()->[0];
}

sub deriv {
	my $self = shift;
	my ($range, $maxlag, $time) = @_;
	$maxlag ||= $range * 2;
	$time ||= time;

	# Extrapolate derivation at $time over given time range $range;
	# refuse if samples are further away than $maxlag.
	my $t1 = $self->retrieve_at($time);
	$t1->[0] >= $time - $maxlag or return undef;
	my $t2 = $self->retrieve_at($t1->[0] - $range);
	$t2->[0] >= $t1->[0] - $range - $maxlag or return undef;

	my $trange = $t1->[0] - $t2->[0];
	# TODO: Buy-sell cross slope?
	my @slope = map { ($t1->[$_] - $t2->[$_]) / $trange } (1, 2);

	# print "$t1->[0], $t2->[0]; $t1->[1], $t2->[1]\n";
	return [time, @slope, $t1, $t2];
}

no Moose;
__PACKAGE__->meta->make_immutable;
