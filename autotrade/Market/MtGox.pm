package autotrade::Market::MtGox;

use Moose;
extends 'autotrade::Market';
use autotrade::MarketTools;

sub _build_fee { 0.0065; }

has 'rcfile' => ( isa => 'Str', is => 'ro', default => '~/.mgrc' );

has 'dummy' => ( isa => 'Int', is => 'rw', default => 0 );

use WebService::MtGox;
has 'mtgox' => ( isa => 'WebService::MtGox', is => 'ro', lazy_build => 1 );
sub _build_mtgox {
	my $self = shift;
	my $rcfile = $self->rcfile();
	my @mgrc = split /\n/, `cat $rcfile`;
	return WebService::MtGox->new(user => $mgrc[0], password => $mgrc[1]);
}

override market_data => sub {
	my $self = shift;
	return autotrade::MarketData::MtGox->new(m => $self);
};

override buy => sub {
	my $self = shift;
	my ($amount, $rate) = @_;
	if ($self->dummy()) {
		print "buy(amount => ".curr_str($amount).", price => ".curr_str($rate).")\n";
	} else {
		$self->mtgox()->buy(amount => curr_str($amount), price => curr_str($rate));
	}
};

override sell => sub {
	my $self = shift;
	my ($amount, $rate) = @_;
	if ($self->dummy()) {
		print "sell(amount => ".curr_str($amount).", price => ".curr_str($rate).")\n";
	} else {
		$self->mtgox()->sell(amount => curr_str($amount), price => curr_str($rate));
	}
};

override cancel => sub {
	my $self = shift;
	my ($o) = @_;
	if ($self->dummy()) {
		print "cancel(oid => ".$o->oid().", type => ".$o->otype().")\n";
	} else {
		$self->mtgox()->cancel(oid => $o->oid(), type => ($o->otype() eq 'sell' ? 1 : 2));
	}
};

no Moose;
__PACKAGE__->meta->make_immutable;


package autotrade::MarketData::MtGox;

use Moose;
extends 'autotrade::MarketData';
use autotrade::MarketTools;

has '+m' => (isa => 'autotrade::Market::MtGox', is => 'ro', required => 1, handles => ['mtgox']);

# Info on our balance and outstanding orders.
has 'status' => (isa => 'HashRef', is => 'ro', lazy_build => 1);
sub _build_status { my $self = shift; $self->mtgox()->list(); }

# Info on rates.
has 'ticker' => (isa => 'HashRef', is => 'ro', lazy_build => 1);
sub _build_ticker { my $self = shift; $self->mtgox()->get_ticker()->{ticker}; }

sub _build_bal_btc { my $self = shift; str_curr($self->status()->{btcs}); }
sub _build_bal_rwm { my $self = shift; str_curr($self->status()->{usds}); }

sub _build_rate_buybtc { my $self = shift; str_curr($self->ticker()->{buy}); }
sub _build_rate_sellbtc { my $self = shift; str_curr($self->ticker()->{sell}); }

sub _build_orders {
	my $self = shift;
	[ map { autotrade::MarketData::Order->new(
		otype => ($_->{type} == 1 ? 'sell' : 'buy'),
		oid => $_->{oid},
		amount => str_curr($_->{amount}),
		rate => str_curr($_->{price}),
		time => $_->{date})
	  } @{$self->status()->{orders}} ];
}

no Moose;
__PACKAGE__->meta->make_immutable;
