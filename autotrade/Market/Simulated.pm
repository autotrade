package autotrade::Market::Simulated;

use Moose;
extends 'autotrade::Market';
use autotrade::MarketTools;

sub _build_fee { 0.0065; }

has '+minbal_rwm' => (isa => 'Curr', is => 'ro', coerce => 1, default => str_curr(0));
has 'bal_btc' => (isa => 'Curr', is => 'rw', 'required' => 1);
has 'bal_usd' => (isa => 'Curr', is => 'rw', 'required' => 1);
has 'rh' => (isa => 'autotrade::RateHistory', is => 'ro', 'required' => 1);

override market_data => sub {
	my $self = shift;
	my ($time) = @_;
	return autotrade::MarketData::Simulated->new(m => $self, time => $time);
};

override buy => sub {
	my $self = shift;
	my ($amount, $rate) = @_;
	my $price = curr_conv($amount, $rate) * (1 + $self->fee());
	if ($price > $self->bal_usd()) {
		printf "! REFUSING BUY: %s BTC @ %s (%s > %s)\n",
			curr_str($amount), curr_str($rate), curr_str($price), curr_str($self->bal_usd());
		return undef;
	}
	$self->bal_usd($self->bal_usd() - $price);
	$self->bal_btc($self->bal_btc() + $amount);
};

override sell => sub {
	my $self = shift;
	my ($amount, $rate) = @_;
	if ($amount > $self->bal_btc()) {
		printf "! REFUSING SELL: %s BTC (> %s)\n",
			curr_str($amount), curr_str($self->bal_btc());
		return undef;
	}
	my $price = curr_conv($amount, $rate) * (1 - $self->fee());
	$self->bal_usd($self->bal_usd() + $price);
	$self->bal_btc($self->bal_btc() - $amount);
};

override cancel => sub {
	my $self = shift;
	my ($o) = @_;
	print "! CANCEL NOT SUPPORTED\n";
};

no Moose;
__PACKAGE__->meta->make_immutable;


package autotrade::MarketData::Simulated;

use Moose;
extends 'autotrade::MarketData';
use autotrade::MarketTools;

has '+m' => (isa => 'autotrade::Market::Simulated', is => 'ro', required => 1);
has 'time' => (isa => 'Int', is => 'ro', required => 1);

# Info on rates.
has 'ticker' => (isa => 'HashRef', is => 'ro', lazy_build => 1);
sub _build_ticker {
	my $self = shift;
	my ($time, $buy, $sell) = @{$self->m()->rh()->retrieve_at($self->time())};
	return { buy => $buy, sell => $sell };
}

sub _build_bal_btc { my $self = shift; $self->m()->bal_btc(); }
sub _build_bal_rwm { my $self = shift; $self->m()->bal_usd(); }

sub _build_rate_buybtc { my $self = shift; $self->ticker()->{buy}; }
sub _build_rate_sellbtc { my $self = shift; $self->ticker()->{sell}; }

sub _build_orders {
	my $self = shift;
	[];
}

no Moose;
__PACKAGE__->meta->make_immutable;
