package autotrade::MarketTools;

use Carp;
use Moose::Util::TypeConstraints;

require Exporter;
our @ISA = qw(Exporter);
our @EXPORT = qw($mult str_curr curr_str curr_conv doublefee_cost);

our $mult = 1000;
sub str_curr { sprintf '%d', $_[0] * $mult; }
sub curr_str { sprintf '%.3f', $_[0] / $mult; }
subtype "Curr", as "Str";
coerce "Curr", from "Str", via { str_curr($_) };
# coerce "Str", from "Curr", via { curr_str($_) };

# Convert from currency to currency by rate.
sub curr_conv {
	my ($amount, $rate) = @_;
	$amount * $rate / $mult;
}

# How much fees we need to pay for a buy-sell combo, assuming we will
# reimburse for the fees within the sell amount.
sub doublefee_cost {
	my ($m, $buy, $sell) = @_;
	# This solves the equation
	# 	(buy + (sell + fees)) * tfee = fees
	# returning $fees.
	return ($buy + $sell) * $m->fee() / (1 - $m->fee());
}

1;
