package autotrade::SQL;

use DBI;

sub db_connect {
	my ($dbfile) = @_;
	$dbfile ||= 'autotrade.sqlite';
	my $dbh = DBI->connect("dbi:SQLite:dbname=$dbfile", "", "")
		or die ("Can't open DB: $!");
	$dbh;
}

1;
