package autotrade::Market;

use Moose;
use autotrade::MarketTools;

# Transaction fee rate. XXX: It's a float, not *$mult fixed-point.
has 'fee' => (isa => 'Num', is => 'ro', required => 1, lazy_build => 1);

# Minimal balance to keep available at the market.
has 'minbal_rwm' => (isa => 'Curr', is => 'ro', coerce => 1, default => str_curr(10));

# Returns autotrade::MarketData instance for semi-current state
# (current, but in part retrieved lazily).
sub market_data { my $self = shift; confess; }

# buy(amount, rate)
# Returns ID of transaction or undef on error.
sub buy { my $self = shift; my ($amount, $rate) = @_; confess; }

# sell(amount, rate)
# Returns ID of transaction or undef on error.
sub sell { my $self = shift; my ($amount, $rate) = @_; confess; }

# cancel(autotrade::MarketData::Order)
sub cancel { my $self = shift; my ($o) = @_; confess; }

1;
