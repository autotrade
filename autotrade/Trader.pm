package autotrade::Trader;

use Moose;

has 'm' => ( isa => 'autotrade::Market', is => 'ro', required => 1 );

has 'buy_lifetime' => ( isa => 'Int', is => 'rw', default => 9999999 );
has 'sell_lifetime' => ( isa => 'Int', is => 'rw', default => 9999999 );

# Single frame; analyze market data and make buys/sells.
# Takes parameters: autotrade::MarketData
# Returns 1 if we should try to make more buys/sells right away.
sub beat {
	my $self = shift;
	my ($md) = @_;

	# The default implementation will just prune old orders.
	# In theory - default lifetimes actually prevent that.
	my $now = time;
	my @o = grep {
			defined $_->time()
			and $_->time() < $now - ($_->otype() eq 'buy' ? $self->buy_lifetime() : $self->sell_lifetime())
		} @{$md->orders()};
	foreach my $o (@o) {
		printf "\tCancelling too old order %d (%d): %s %s BTC @ %s USD\n",
			$o->oid(), $o->time(), $o->otype(), $o->amount(), $o->rate();
		$self->m()->cancel($o);
	}

	return 0;
}

1;
