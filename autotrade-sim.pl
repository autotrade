#!/usr/bin/perl
#
# Automated Bitcoin Trader - Simulator
# (c) Petr Baudis <pasky@ucw.cz>  2011
#
# Feeds the trader simulated data based on RateHistory database. You can
# import historical data using
#	curl 'http://bitcoincharts.com/t/trades.csv?start=0&end='$(date +%s)'&symbol=mtgoxUSD' | ./import-csv.pl
#
# You can render the generated plot using
#	gnuplot -persist -e 'plot "sim.plot" using 2 with lines title "buy", "sim.plot" using 3 with lines title "sell", "sim.plot" using 4 with lines title "btc", "sim.plot" using 5 with lines title "usd" axes x1y2, "sim.plot" using 6 with lines title "total" axes x1y2'
#
# Usage: ./autotrate-sim.pl START_BALANCE STARTTIME STEP ENDTIME

use warnings;
use strict;


use autotrade::MarketTools;

my ($usdbal, $start, $step, $stop) = @ARGV;

use autotrade::SQL;
use autotrade::RateHistory;
use autotrade::Market::Simulated;
use autotrade::Trader::LowHigh;
use autotrade::Trader::Slope;

my $dbh = autotrade::SQL::db_connect("autotrade-sim.sqlite");
$dbh->do("PRAGMA synchronous = OFF");
$dbh->do("DELETE FROM bitcoin_values");
my $rh = autotrade::RateHistory->new(dbh => $dbh);
my $m = autotrade::Market::Simulated->new(bal_btc => 0, bal_usd => str_curr($usdbal), rh => $rh);
my $t = autotrade::Trader::Slope->new(dbh => $dbh, rh => $rh, m => $m, time_window => 1200, raise_threshold => 0.25, fall_threshold => -0.25);

open PLOT, ">sim.plot";

while ($start < $stop) {
	printf "%d: %s\n", $start, scalar localtime($start);
	my ($md, $n);
	do {
		$md = $m->market_data($start);
	} while ($t->beat($md, $start));
	printf PLOT "%d %s %s %s %s %s\n", time,
		curr_str($md->rate_buybtc()), curr_str($md->rate_sellbtc()),
		curr_str($m->bal_btc()), curr_str($m->bal_usd()),
		curr_str(curr_conv($m->bal_btc(), $md->rate_buybtc()) + $m->bal_usd());
	$start += $step;
	print "\n";
}

printf "=== Final balance: %s BTC, %s USD => %s USD\n",
	curr_str($m->bal_btc()), curr_str($m->bal_usd()), curr_str(curr_conv($m->bal_btc(), $m->market_data($start)->rate_buybtc()) + $m->bal_usd());
