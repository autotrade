#!/usr/bin/perl
#
# Automated Bitcoin Trader
# (c) Petr Baudis <pasky@ucw.cz>  2011

use warnings;
use strict;


use autotrade::MarketTools;

use Getopt::Std;
our ($opt_b, $opt_r, $opt_i);
getopt('b:r:i:');

my $interval = $opt_i; # [s] trade cycle period
$interval ||= 10;

my %mtgox_opts = ();
defined $opt_b and $mtgox_opts{minbal_rwm} = str_curr($opt_b);
defined $opt_r and $mtgox_opts{rcfile} = $opt_r;


use autotrade::SQL;
use autotrade::RateHistory;
use autotrade::Market::MtGox;
use autotrade::Trader::LowHigh;
use autotrade::Trader::Slope;

my $dbh = autotrade::SQL::db_connect();
my $rh = autotrade::RateHistory->new(dbh => $dbh);
my $m = autotrade::Market::MtGox->new(%mtgox_opts);
my $t = autotrade::Trader::Slope->new(dbh => $dbh, rh => $rh, m => $m);


while (1) {
	print "... ".localtime." check\n";
	my $md;
	do {
		$md = $m->market_data();
		$rh->record(time, $md->rate_buybtc(), $md->rate_sellbtc());
	} while ($t->beat($md));
	print "... ".localtime." check finished.\n\n";
	sleep $interval;
}
